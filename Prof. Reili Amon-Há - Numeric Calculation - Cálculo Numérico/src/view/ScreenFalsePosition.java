package view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controller.FalsaPosicao;

public class ScreenFalsePosition extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = -4251559400315566683L;
	private JLabel lbError = new JLabel("Informe o erro: ");
	private GridBagConstraints l = new GridBagConstraints();
	private JPanel pnErro = new JPanel(new GridBagLayout()),
						pnEquacao = new JPanel(new GridBagLayout());
	private JTextField txtError = new JTextField (5);
	private JButton calculate = new JButton("Calcular"),
					btBack = new JButton("Voltar");
	private JRadioButton first = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/false position equation1.png"))),
									second = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/false position equation2.png")));
	private JRadioButton [] images = {first, second};
	private ButtonGroup grupo = new ButtonGroup();
	
	public ScreenFalsePosition(){
		super("M�todo da Falsa Posi��o");
		setLayout(new GridBagLayout());
		
		l.insets = new Insets(3,3,3,3);
		l.gridx = 0;
		l.gridy = 0;
		pnErro.add(lbError, l);
		
		pnErro.setBorder(BorderFactory.createEtchedBorder());
		
		grupo.add(images[0]);
		grupo.add(images[1]);
		
		l.gridx = 0;
		l.gridy = 0;
		pnEquacao.add(images[0], l);
		l.gridy = 1;
		pnEquacao.add(images[1], l);
		
		
		images[0].addActionListener(this);
		images[1].addActionListener(this);
		
		l.gridx = 0;
		l.gridy = 0;
		l.anchor = GridBagConstraints.WEST;
		l.gridx = 1;
		pnErro.add(txtError, l);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridx = 0;
		l.gridy = 0;
		add(pnErro, l);
		l.gridy = 1;
		add(pnEquacao, l);
		l.gridy = 2;
		add(calculate, l);
		l.gridy = 3;
		add(btBack, l);
		
		calculate.addActionListener(this);
		btBack.addActionListener(this);
		
		setVisible(true);
		setSize(300, 300);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent e) {
		
		for(int i=0; i<images.length; i++)
			images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/false position equation"+(i+1)+".png")));
		
		if(e.getSource() == calculate){
			if(!(txtError.getText().trim().equals(""))){
				if(images[0].isSelected() == true)
					FalsaPosicao.metodoFalsePosition(1, Double.parseDouble(txtError.getText()));
				else
					FalsaPosicao.metodoFalsePosition(2, Double.parseDouble(txtError.getText()));
			}
			else{
				JOptionPane.showMessageDialog(null, "Informe um erro v�lido.");
				txtError.setText("");
			}
		}
		
		if(e.getSource() == btBack){
			dispose();
			MainCalculation.screen.setVisible(true);
		}
		
		for(int i=0; i<images.length; i++){
			if(e.getSource() == images[i]){
				images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/false position equation"+(i+1)+"."+(i+1)+".png")));
				break;
			}
		}
	}

}
