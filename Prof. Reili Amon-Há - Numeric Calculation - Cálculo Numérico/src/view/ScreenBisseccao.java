package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import controller.Bisseccao;

public class ScreenBisseccao extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JMenu menuExit = new JMenu("Sair");
	private JMenuItem menuItemExit = new JMenuItem("Sair");
	private JButton btCal = new JButton("Calcular"),
					btBack = new JButton("Voltar");
	private JPanel pnEquacao = new JPanel(new GridBagLayout()),
						pnErro = new JPanel(new GridBagLayout());
	private JLabel lbIntervalo = new JLabel("Escolha a equa��o: "),
						lbError = new JLabel("Informe o erro: ");
	private JRadioButton first = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 1.png"))),
									second = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 2.png"))),
									third = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 3.png"))),
									fourth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 4.png"))),
									fiveth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 5.png"))),
									sixth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 6.png"))),
									seventh = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 7.png"))),
									eighth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 8.png"))),
									ninth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 9.png"))),
									tenth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 10.png"))),
									eleventh = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 11.png"))),
									twelfth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 12.png"))),
									thirteenth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 13.png"))),
									fourteenth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 14.png"))),
									fiveteenth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/equation 15.png")));
	private JRadioButton [] images = {first, second, third, fourth, fiveth, sixth, seventh, eighth, ninth, tenth, eleventh, twelfth, thirteenth, fourteenth, fiveteenth};
	private ButtonGroup grupo = new ButtonGroup();
	private JTextField txtError = new JTextField (5);

	public ScreenBisseccao(){
		super("M�todo de Bissec��o");
		
		setLayout(new GridBagLayout());
		
		createMenu();
		createMenuForm();
		
		pnEquacao.setBorder(BorderFactory.createEtchedBorder());
		pnErro.setBorder(BorderFactory.createEtchedBorder());
		
		setVisible(true);
		setSize(800, 500);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	private void createMenu(){

		menuExit.add(menuItemExit);
		menuItemExit.addActionListener(this);

		JMenuBar barra = new JMenuBar();
		setJMenuBar(barra);

		barra.add(menuExit);
		
	}
	public void createMenuForm(){
		
		GridBagConstraints l = new GridBagConstraints();
		
		for(int i=0; i<images.length; i++)
			grupo.add(images[i]);
		
		int x = 1;
		for(int i=0; i<images.length; i++){
			if(images[i] == sixth || images[i] == seventh || images[i] == eighth || images[i] == ninth || images[i] == tenth){
				l.gridx = 1;
				l.gridy = x++;
			} else if(images[i] == eleventh || images[i] == twelfth || images[i] == thirteenth || images[i] == fourteenth || images[i] == fiveteenth){
				l.gridx = 2;
				l.gridy = x-5;
				x++;
			}	else {
				l.gridx = 0;
				l.gridy = i+1;
			}
			pnEquacao.add(images[i], l);
			
			images[i].addActionListener(this);
		}
	
		btCal.addActionListener(this);
		btBack.addActionListener(this);
		
		l.insets = new Insets(3,3,3,3);
		l.gridx = 0;
		l.gridy = 0;
		pnErro.add(lbError, l);
		l.gridx = 1;
		pnErro.add(txtError, l);
		
		l.gridx = 0;
		l.gridy = 0;
		l.anchor = GridBagConstraints.CENTER;
		add(lbIntervalo, l);
		l.anchor = GridBagConstraints.WEST;
		add(pnErro, l);
		l.anchor = GridBagConstraints.CENTER;
		l.gridy = 1;
		add(pnEquacao, l);
		l.gridy = 2;
		add(btCal, l);
		l.gridy = 3;
		add(btBack, l);
		
	}
	public void setAllImages(){
		for(int i=0; i<images.length; i++)
			images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/equation "+(i+1)+".png")));
	}
	public void actionPerformed(ActionEvent e) {
		
		setAllImages();
		
		if(e.getSource() == menuItemExit)
			System.exit(0);
		
		if(e.getSource() == btCal){
			if(txtError.getText().equals(""))
				JOptionPane.showMessageDialog(null, "Informe o erro!");
			else {
				for(int i=0; i<images.length; i++){
					if(i==0 || i==1 || i==3 || i==5 || i==8 || i==7 || i==9 || i==11 || i==12 || i==14){
						if(images[i].isSelected()){
							JOptionPane.showMessageDialog(null, Bisseccao.metodoBisseccao2Roots(i+1, Double.parseDouble(txtError.getText())));
							break;
						}
					} else {
						if(images[i].isSelected()){
							JOptionPane.showMessageDialog(null, Bisseccao.metodoBisseccao3Roots(i+1, Double.parseDouble(txtError.getText())));
							break;
						}
					}
				}
			}
		}
		
		if(e.getSource() == btBack){
			dispose();
			MainCalculation.screen.setVisible(true);
		}
		
		for(int i=0; i<images.length; i++){
			if(e.getSource() == images[i]){
				images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/equation "+(i+1)+"."+(i+1)+".png")));
				break;
			}
		}
		
	}
	
}