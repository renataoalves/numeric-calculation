package view;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class AboutTaylor extends JFrame {
	
	private static final long serialVersionUID = 1L;

	public AboutTaylor (){
		super("Brook Taylor");
		
		JTextArea textArea = new JTextArea("  Nascido em Edmonton, Middlesex, Inglaterra, a 18 de Agosto de 1685 e falecido em Londres,"
				+ "Inglaterra a 29 de Dezembro de 1731. Brook Taylor era filho de John Taylor da Casa de Bifrons e de Olivia,"
				+ "filha de Nicholas Tempest. Sua fam�lia era moderadamente rica e estava ligada � baixa nobreza. Seu av�, Nathaniel,"
				+ "tinha apoiado Oliver Cromwell. John Taylor era um pai severo e rigoroso, o expulsou de casa em 1721 quando Taylor decidiu se casar com uma mulher que,"
				+ "embora pertencesse a uma boa fam�lia, n�o era muito rica. Em 1723 Brook voltou � sua casa ap�s a morte de sua esposa durante o parto."
				+ "Ele se casou novamente em 1725, desta vez com a aprova��o e ben��o de seu pai, mas infelizmente sua segunda mulher tamb�m morreu durante o parto em 1730."
				+ "Sua filha, entretanto, conseguiu sobreviver. A vida pessoal de Taylor parece ter influenciado seu trabalho em diversas formas."
				+ "Duas das suas maiores contribui��es cient�ficas lidam com vibra��es e desenho em perspectiva. Seu pai era muito interessado em m�sica e artes,"
				+ "sua casa estava sempre cheia de artistas. Os arquivos da fam�lia cont�m pinturas de Taylor e tamb�m um manuscrito n�o publicado chamado On Musick foi"
				+ " encontrado entre seus pap�is no Saint John's College em Cambridge. Taylor teve aulas particulares em casa antes de entrar para o Saint John's College em 1701,"
				+ " onde os catedr�ticos em matem�tica eram John Machin e John Keill. Taylor recebeu seu diploma de Bacharelado em 1709, foi eleito para a"
				+ "Royal Society de Londres em 1712 e recebeu o diploma de Doutorado em 1714. Ele foi eleito secret�rio da Royal Society em janeiro de 1714,"
				+ "mas se demitiu em outubro de 1718 em virtude de sua sa�de e talvez tamb�m pela perda de interesse nesta tarefa cansativa e extenuante. ", 10, 10);
		
		textArea.setFont(new Font("Vedana", Font.PLAIN, 14));
		textArea.setBackground(Color.white);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scroll);
		
		setVisible(true);
		setSize(700, 300);
		setResizable(false);
		setLocationRelativeTo(null);
	}
}