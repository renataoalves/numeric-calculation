package view;

import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.TitledBorder;

import controller.Secante;

public class ScreenSecante extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JLabel lblInformeOErro;
	private JTextField txtErro;
	private JButton btnCalcular, btnVoltar;
	private JRadioButton first = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/secante 1.png"))),
									second = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/secante 2.png"))),
									third = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/secante 3.png")));
	private JRadioButton [] images = {first, second, third};
	private int equacao;
	private JPanel panelEquation, panelOthers;
	private JTextField txtX0;
	private JTextField txtX1;
	
	public ScreenSecante() {
		setTitle("M�todo da Secante");
		getContentPane().setLayout(null);
		
		panelOthers = new JPanel(null);
		panelOthers.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		lblInformeOErro = new JLabel("INFORME O ERRO:");
		lblInformeOErro.setBounds(16,11,107,14);
		panelOthers.add(lblInformeOErro);
		
		txtErro = new JTextField();
		txtErro.setBounds(124,10,66,20);
		panelOthers.add(txtErro);
		
		panelEquation = new JPanel(null);
		panelEquation.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		first.setBounds(85, 7, 176, 64);
		second.setBounds(83, 74, 178, 58);
		third.setBounds(83, 136, 178, 65);
		panelEquation.add(first);
		panelEquation.add(second);
		panelEquation.add(third);
		
		panelOthers.setBounds(9, 5, 329, 60);
		getContentPane().add(panelOthers);
		
		txtX0 = new JTextField();
		txtX0.setBounds(242, 9, 66, 20);
		panelOthers.add(txtX0);
		
		txtX1 = new JTextField();
		txtX1.setBounds(242, 32, 66, 20);
		panelOthers.add(txtX1);
		
		JLabel lblX0 = new JLabel("x0.");
		lblX0.setBounds(216, 12, 32, 14);
		panelOthers.add(lblX0);
		
		JLabel lblX1 = new JLabel("x1.");
		lblX1.setBounds(215, 34, 32, 14);
		panelOthers.add(lblX1);
		
		panelEquation.setBounds(9, 74, 328, 204);
		getContentPane().add(panelEquation);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(9, 285, 329, 43);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		btnCalcular = new JButton("CALCULAR");
		btnCalcular.setBounds(167, 11, 150, 20);
		panel.add(btnCalcular);
		
		btnVoltar = new JButton("VOLTAR");
		btnVoltar.setBounds(11, 11, 150, 20);
		panel.add(btnVoltar);
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				MainCalculation.screen.setVisible(true);
			}
		});
		btnCalcular.addActionListener(this);
		
		first.addActionListener(this);
		second.addActionListener(this);
		third.addActionListener(this);
		
		setVisible(true);
		setSize(354, 368);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void setAllImages(){
		for(int i=0; i<images.length; i++)
			images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/secante "+(i+1)+".png")));
	}

	public void actionPerformed(ActionEvent e) {
		setAllImages();
		
		try{
			if(e.getSource() == btnCalcular){
				Double.parseDouble(txtErro.getText().trim().replace(',','.'));
				Double.parseDouble(txtX0.getText().trim().replace(',','.'));
				Double.parseDouble(txtX1.getText().trim().replace(',','.'));
				
				if(txtErro.getText().trim().equals(""))
					JOptionPane.showMessageDialog(null, "Informe o erro!", "ERRO", 0);
				else if(txtX0.getText().trim().equals(""))
					JOptionPane.showMessageDialog(null, "Informe o X0!", "ERRO", 0);
				else if(txtX1.getText().trim().equals(""))
					JOptionPane.showMessageDialog(null, "Informe o X1!", "ERRO", 0);
				else{
					if(equacao == 0)
						JOptionPane.showMessageDialog(null, "Escolha uma equa��o.", "ERRO", 0);
					else
						Secante.calculaSecante(equacao, Double.parseDouble(txtX0.getText().trim().replace(',','.')), Double.parseDouble(txtX1.getText().trim().replace(',','.')), Double.parseDouble(txtErro.getText()));
				}
				equacao = 0;
			}
		} catch (Exception ex){
			JOptionPane.showMessageDialog(null, "Informe um erro v�lido!", "ERRO", 0);
		}
		
		for(int i=0; i<images.length; i++){
			if(e.getSource() == images[i]){
				equacao = i+1;
				images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/secante "+(i+1)+"."+(i+1)+".png")));
				break;
			}
		}
	}
}