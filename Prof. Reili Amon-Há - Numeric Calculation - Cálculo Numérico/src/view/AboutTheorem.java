package view;

import java.awt.*;
import javax.swing.*;

public class AboutTheorem extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	public AboutTheorem (){
		super("Sobre o Polin�mio de Taylor");
		setLayout(new FlowLayout());
		
		JTextArea textArea = new JTextArea("	Este teorema permite aproximar uma fun��o deriv�vel na vizinhan�a reduzida em torno de"
				+ " um ponto a: E (a, d) mediante um polin�mio cujos coeficientes dependem das derivadas da fun��o nesse "
				+ "ponto. Em termos matem�ticos: Se n >= 0 � um inteiro e (f) uma fun��o que � deriv�vel n vezes no "
				+ "intervalo fechado [a,  x] e n+1 no intervalo aberto ] a,  x[", 5, 40);
		
		textArea.setFont(new Font("Verdana", Font.PLAIN, 14));
		textArea.setBackground(Color.white);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		
		GridBagConstraints x = new GridBagConstraints();
		x.gridx = 0;
		x.gridy = 0;
		//panel.add(textArea, x);
		add(textArea);
		
		setSize(540, 150);
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
	}
}
