package view;

import javax.swing.*;

import controller.Newton;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ScreenNewton extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JLabel lblInformeOErro;
	private JTextField txtErro;
	private JButton btnCalcular, btnVoltar;
	private JRadioButton first = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 1.png"))),
			second = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 2.png"))),
			third = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 3.png"))),
			fourth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 4.png"))),
			fiveth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 5.png"))),
			sixth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 6.png"))),
			seventh = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 7.png"))),
			eighth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 8.png"))),
			ninth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 9.png"))),
			tenth = new JRadioButton(new ImageIcon(getClass().getResource("/imagem/newton 10.png")));
	private JRadioButton [] images = {first, second, third, fourth, fiveth, sixth, seventh, eighth, ninth, tenth};
	private int equacao;
	
	public ScreenNewton() {
		setTitle("M�todo de Newton");
		getContentPane().setLayout(null);
		
		lblInformeOErro = new JLabel("INFORME O ERRO:");
		lblInformeOErro.setBounds(10, 11, 104, 14);
		getContentPane().add(lblInformeOErro);
		
		txtErro = new JTextField();
		txtErro.setBounds(116, 10, 77, 19);
		getContentPane().add(txtErro);
		
		btnCalcular = new JButton("CALCULAR");
		btnCalcular.addActionListener(this);
		btnCalcular.setBounds(203, 7, 104, 23);
		getContentPane().add(btnCalcular);
		
		btnVoltar = new JButton("VOLTAR");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				MainCalculation.screen.setVisible(true);
			}
		});
		btnVoltar.setBounds(317, 7, 89, 23);
		getContentPane().add(btnVoltar);
		
		JPanel panelEquation = new JPanel(new GridBagLayout());
		GridBagConstraints g = new GridBagConstraints();
		g.gridx = 0;
		g.gridy = 0;
		panelEquation.add(first, g);
		g.gridy = 1;
		panelEquation.add(second, g);
		g.gridy = 2;
		panelEquation.add(third, g);
		g.gridy = 3;
		panelEquation.add(fourth, g);
		
		g.gridx = 1;
		g.gridy = 0;
		panelEquation.add(sixth, g);
		g.gridy = 1;
		panelEquation.add(seventh, g);
		g.gridy = 2;
		panelEquation.add(eighth, g);
		g.gridy = 3;
		panelEquation.add(ninth, g);
		
		g.gridx = 0;
		g.gridy = 4;
		g.gridwidth = 2;
		panelEquation.add(tenth, g);
		g.gridy = 5;
		panelEquation.add(fiveth, g);
		
		panelEquation.setBounds(10, 35, 787, 385);
		getContentPane().add(panelEquation);
		
		first.addActionListener(this);
		second.addActionListener(this);
		third.addActionListener(this);
		fourth.addActionListener(this);
		fiveth.addActionListener(this);
		sixth.addActionListener(this);
		seventh.addActionListener(this);
		eighth.addActionListener(this);
		ninth.addActionListener(this);
		tenth.addActionListener(this);
		
		setVisible(true);
		setResizable(false);
		setSize(750, 470);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void setAllImages(){
		for(int i=0; i<images.length; i++)
			images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/newton "+(i+1)+".png")));
	}

	public void actionPerformed(ActionEvent e) {
		
		setAllImages();
		
		if(e.getSource() == btnCalcular){
			if(txtErro.getText().trim().equals(""))
				JOptionPane.showMessageDialog(null, "Informe o erro!", "ERRO", 0);
			else
				Newton.metodoCalcularNewton(equacao, txtErro.getText());
			
		}
		
		for(int i=0; i<images.length; i++){
			if(e.getSource() == images[i]){
				equacao = i+1;
				images[i].setIcon(new ImageIcon(getClass().getResource("/imagem/newton "+(i+1)+".1.png")));
				break;
			}
		}
	}

}
