package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import controller.PolinomioTaylor;

public class ScreenTaylor extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JLabel lbValue, e, lbPoint, lbConstant, decimalLabel, functionRule, tittle, lbTurn;
	private JTextField txtPoint, txtConstant, txtDecimal, txtTurn;
	private JButton buttonCalculate, buttonCleanUp,
			btRule = new JButton("Regra"),
			btPoly = new JButton("Polin�mio"),
			btResulPoly = new JButton("Resultado"),
			btValueReal = new JButton("Valor Real"),
			btErro = new JButton("Erro"),
			btBack = new JButton("Voltar ao menu");
	private GridBagConstraints l = new GridBagConstraints();
	private NewAction newAction = new NewAction();
	private JPanel panelTittle = new JPanel(),
			panelRegister = new JPanel(new GridBagLayout()),
			panelButtons = new JPanel(new GridBagLayout());
	
	public ScreenTaylor(){
		super("M�todo - Polin�mio de Taylor");

		createMenu();
		createMenuForm();
		
		btRule.addActionListener(newAction);
		btPoly.addActionListener(newAction);
		btResulPoly.addActionListener(newAction);
		btValueReal.addActionListener(newAction);
		btErro.addActionListener(newAction);
		btBack.addActionListener(newAction);
		
		setVisible(true);
		setSize(850, 350);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void createMenu(){

		CloseAction closeAction = new CloseAction();
		AboutActionTaylor aboutActionTaylor = new AboutActionTaylor();
		AboutActionTheorem aboutActionTheorem = new AboutActionTheorem();
		
		JMenu menuHelp = new JMenu("Ajuda");

		JMenuItem menuItemAboutTaylor = new JMenuItem("Brook Taylor");
		menuHelp.add(menuItemAboutTaylor);
		menuItemAboutTaylor.addActionListener(aboutActionTaylor);

		JMenuItem menuItemAboutTheorem = new JMenuItem("Polin�mio de Taylor");
		menuHelp.add(menuItemAboutTheorem);
		menuItemAboutTheorem.addActionListener(aboutActionTheorem);

		JMenu menuExit = new JMenu("Sair");
		
		JMenuItem menuItemExit = new JMenuItem("Sair");
		menuExit.add(menuItemExit);
		menuItemExit.addActionListener(closeAction);

		JMenuBar barra = new JMenuBar();
		setJMenuBar(barra);

		barra.add(menuHelp);
		barra.add(menuExit);

	}

	private void createMenuForm(){

		setLayout(new GridBagLayout());

		panelTittle.setLayout(new FlowLayout());

		tittle = new JLabel("C�lculo do Polin�mio de Taylor");
		tittle.setFont(new Font("Times New Roman", Font.PLAIN, 25));

		panelTittle.add(tittle);

		l.insets = new Insets (2,2,2,2);
		
		lbValue = new JLabel("Fun��o: ");
		e = new JLabel(new ImageIcon(getClass().getResource("/imagem/e^x.png")));
		lbPoint = new JLabel("Ponto (X): ");
		lbConstant = new JLabel("Constante (Xo): ");
		lbTurn = new JLabel("Grau (N): ");
		decimalLabel = new JLabel("Qnt. de casas decimais: ");
		
		lbValue.setFont(new Font("Times new Roman", Font.PLAIN, 20));
		lbPoint.setFont(new Font("Times new Roman", Font.PLAIN, 20));
		lbTurn.setFont(new Font("Times new Roman", Font.PLAIN, 20));
		lbConstant.setFont(new Font("Times new Roman", Font.PLAIN, 20));
		decimalLabel.setFont(new Font("Times new Roman", Font.PLAIN, 20));
		
		txtPoint = new JTextField(5);
		txtTurn = new JTextField(5);
		txtConstant = new JTextField(5);
		txtDecimal = new JTextField(5);
		
		l.anchor = GridBagConstraints.EAST;
		l.gridx = 0;
		l.gridy = 0;
		panelRegister.add(lbValue, l);
		l.gridy = 1;
		panelRegister.add(lbPoint, l);
		l.gridy = 2;
		panelRegister.add(lbTurn, l);
		l.gridy = 3;
		panelRegister.add(lbConstant, l);
		l.gridy = 4;
		panelRegister.add(decimalLabel, l);
		
		l.anchor = GridBagConstraints.WEST;
		l.gridx = 1;
		l.gridy = 0;
		panelRegister.add(e, l);
		l.gridy = 1;
		panelRegister.add(txtPoint, l);
		l.gridy = 2;
		panelRegister.add(txtTurn, l);
		l.gridy = 3;
		panelRegister.add(txtConstant, l);
		l.gridy = 4;
		panelRegister.add(txtDecimal, l);
		
		buttonCalculate = new JButton("Calcular");
		buttonCalculate.setToolTipText("Efetuar c�lculo");
		buttonCalculate.addActionListener(newAction);

		buttonCleanUp = new JButton("Limpar");
		buttonCleanUp.setToolTipText("Limpar todos os campos");
		buttonCleanUp.addActionListener(newAction);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridx = 0;
		l.gridy = 0;
		panelButtons.add(buttonCalculate, l);
		l.gridy = 1;
		panelButtons.add(buttonCleanUp, l);

		l.gridx = 0;
		l.gridy = 1;
		add(panelRegister, l);
		l.gridx = 1;
		functionRule = new JLabel(new ImageIcon(getClass().getResource("/imagem/funcaoo.png")));
		add(functionRule, l);
		
		l.gridy = 2;
		add(btBack, l);
		
		l.anchor = GridBagConstraints.CENTER;
		l.gridy = 0;
		l.gridx = 1;
		add(panelTittle, l);
		l.gridx = 2;
		l.gridy = 1;
		add(panelButtons, l);
	}

	private class NewAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() ==  buttonCleanUp){
				
				panelButtons.removeAll();
				
				l.gridx = 0;
				l.gridy = 0;
				panelButtons.add(buttonCalculate, l);
				l.gridy = 1;
				panelButtons.add(buttonCleanUp, l);

				panelButtons.revalidate();
				
				txtPoint.setEnabled(true);
				txtTurn.setEnabled(true);
				txtConstant.setEnabled(true);
				txtDecimal.setEnabled(true);
				
				txtPoint.setText("");
				txtConstant.setText("");
				txtDecimal.setText("");
				txtTurn.setText("");
			}

			if(e.getSource() ==  buttonCalculate){
				if (!(txtPoint.getText().equals("")) && !(txtTurn.getText().equals("")) && !(txtConstant.getText().equals(""))){

					if(txtDecimal.getText().equals("") || Integer.parseInt(txtDecimal.getText()) < 1)
						txtDecimal.setText("4");
					
					panelButtons.removeAll();
					
					txtPoint.setEnabled(false);
					txtTurn.setEnabled(false);
					txtConstant.setEnabled(false);
					txtDecimal.setEnabled(false);
					
					l.gridx = 0;
					l.gridy = 0;
					panelButtons.add(btRule, l);
					btRule.setToolTipText("A regra de acordo com o grau");
					l.gridy = 1;
					panelButtons.add(btPoly, l);
					btPoly.setToolTipText("Polin�mio formado de acordo com o grau");
					l.gridy = 2;
					panelButtons.add(btResulPoly, l);
					btResulPoly.setToolTipText("Resultado final do polin�mio");
					l.gridy = 3;
					panelButtons.add(btValueReal, l);
					btValueReal.setToolTipText("Valor real da fun��o");
					l.gridy = 4;
					panelButtons.add(btErro, l);
					btErro.setToolTipText("A diferen�a entre o valor real e resultado da fun��o");
					l.gridy = 5;
					panelButtons.add(buttonCleanUp, l);
	
					panelButtons.revalidate();
					
					PolinomioTaylor.polinomioDeTaylor(
							Integer.parseInt(txtPoint.getText()),
							Integer.parseInt(txtTurn.getText()),
							Integer.parseInt(txtConstant.getText()), 
							Integer.parseInt(txtDecimal.getText()));
					
				} else 
					JOptionPane.showMessageDialog(null, "Por favor insira os valores de Ponto, Grau e Constante.", "Error", 2);
			}
			
			if(e.getSource() == btBack){
				dispose();
				MainCalculation.screen.setVisible(true);
			}
			
			if(e.getSource() ==  btRule)
				JOptionPane.showMessageDialog(null, "Regra de acordo com o grau: "+PolinomioTaylor.rule);
			
			if(e.getSource() ==  btPoly)
				JOptionPane.showMessageDialog(null, ""+PolinomioTaylor.writePoli);
				
			if(e.getSource() ==  btResulPoly)
				JOptionPane.showMessageDialog(null, ""+PolinomioTaylor.resultPoli);
				
			if(e.getSource() ==  btValueReal)
				JOptionPane.showMessageDialog(null, ""+PolinomioTaylor.realValue);
			
			if(e.getSource() ==  btErro)
				JOptionPane.showMessageDialog(null, ""+PolinomioTaylor.error);
				
		}
	}

	private class CloseAction implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			JOptionPane.showMessageDialog(null, "Obrigado por escolher nosso Software.", "Agradecimento", JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}
	}

	private class AboutActionTaylor implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			new AboutTaylor();
		}
	}

	private class AboutActionTheorem implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			new AboutTheorem();
		}
	}
}