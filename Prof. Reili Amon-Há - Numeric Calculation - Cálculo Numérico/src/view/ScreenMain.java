package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class ScreenMain extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	private JButton calcTaylor = new JButton ("Polin�mio de Taylor"),
							calcBissec = new JButton ("M�todo de Bissec��o"),
							calcFalse = new JButton ("M�todo da Falsa Posi��o"),
							calcNewton = new JButton ("M�todo de Newton"),
							calcSecante = new JButton ("M�todo da Secante");
	private JPanel panelButton = new JPanel(new GridBagLayout());
	private GridBagConstraints l;
	
	public ScreenMain() {
		super("Escolha o tipo de c�lculo");
		setLayout(new GridBagLayout());
		
		l = new GridBagConstraints();
		l.gridx = 0;
		l.gridy = 0;
		l.insets = new Insets(5,0,0,0);
		
		panelButton.add(calcTaylor, l);
		l.gridy = 1;
		panelButton.add(calcBissec, l);
		l.gridy = 2;
		panelButton.add(calcFalse, l);
		l.gridy = 3;
		panelButton.add(calcNewton, l);
		l.gridy = 4;
		panelButton.add(calcSecante, l);
		
		l.gridx = 0;
		l.gridy = 0;
		add(panelButton,l);
		
		calcBissec.addActionListener(this);
		calcTaylor.addActionListener(this);
		calcFalse.addActionListener(this);
		calcNewton.addActionListener(this);
		calcSecante.addActionListener(this);
		
		setVisible(true);
		setSize(300,200);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == calcTaylor)
			new ScreenTaylor();
		
		if(e.getSource() == calcBissec)
			new ScreenBisseccao();
		
		if(e.getSource() == calcFalse)
			new ScreenFalsePosition();
		
		if(e.getSource() == calcNewton)
			new ScreenNewton();
		
		if(e.getSource() == calcSecante)
			new ScreenSecante();
		
		MainCalculation.screen.setVisible(false);
	}

}