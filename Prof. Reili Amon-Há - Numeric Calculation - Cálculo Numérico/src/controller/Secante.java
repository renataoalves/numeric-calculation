package controller;

import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class Secante {

	public static void main(String[] args) {
		//        EQUA��O, X0, X1, ERRO
		calculaSecante(1, 1.5, 1.7, 10e-2);
	}
	
	public static void calculaSecante(int equation, double xN, double xN_1, double erro){

		double x[] = new double [10];
		x[0] = xN;
		x[1] = xN_1;
		int v = 2;
		
		do{
			x[v] = fnSecante(equation, x[v-2], x[v-1]);
			v++;
			if(v == x.length)
				break;
		}while(Math.abs(x[v] - x[v-1]) >= erro);
		
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("0.00000");
		
		String aux = "";
		
		try{
			for(int i=0; i < x.length; i++){
				aux = df.format(x[i]).replace(',','.');
				x[i] = Double.parseDouble(aux);
			}
		}catch(Exception e){
			System.out.println("Error - fnSecante()");
		}
		
		aux = "Intervalos:\n";
		for(int i=1; i < x.length; i++){
			if(x[i-1] == x[i]){
				aux += "x"+(i-1)+" "+x[i-1]+"\n";
				break;
			}else
				aux += "x"+(i-1)+" "+x[i-1]+"\n";
			if(equation == 3 && i == 5)
				break;
		}
		
		JOptionPane.showMessageDialog(null, aux, "Intervalos", -1);
	}
	
	private static double fnSecante(int equation, double xN, double xN_1){
		if(equation == 1)
			return (xN - (((xN - xN_1)*fnFirst(xN))/(fnFirst(xN) - fnFirst(xN_1))));
		else if(equation == 2)
			return (xN - (((xN - xN_1)*fnSecond(xN))/(fnSecond(xN) - fnSecond(xN_1))));
		else
			return (xN - (((xN - xN_1)*fnThird(xN))/(fnThird(xN) - fnThird(xN_1))));
	}
	
	private static double fnFirst(double x){
		return (Math.pow(x, 2) + x + 6);
	}
	private static double fnSecond(double x){
		return (3*x - Math.cos(x));
	}
	private static double fnThird(double x){
		return (Math.pow(x, 3) - 4);
	}

}