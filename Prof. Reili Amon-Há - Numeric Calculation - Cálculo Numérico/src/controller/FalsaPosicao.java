package controller;

import javax.swing.JOptionPane;

public class FalsaPosicao {
	
public static void metodoFalsePosition(int equation, double error){
		
		double intervalo [] = new double[2];
		String function = "", breaks = "";
		double comparar1 = 0, compararN_1 = 0, fixo = 0;
		
		// Intervalos j� definidos (testados antes) 
		if(equation == 1){ // Para primeira fun��o
			intervalo[0] = 1;
			intervalo[1] = 2; 
		} else { // Para segunda fun��o
			intervalo[0] = -2;
			intervalo[1] = -1;
		}
				
		if(equation == 1){
			if(fnFalseFirstDerivada2(intervalo[0]) < 0 && fnFalseFirstDerivada2(intervalo[1]) < 0)
				function = "f''(x) < 0";
			else
				function = "f''(x) > 0";
		}else{
			if(fnFalseSecondDerivada2(intervalo[0]) < 0 && fnFalseSecondDerivada2(intervalo[1]) < 0)
				function = "f''(x) < 0";
			else
				function = "f''(x) > 0";
		}
		
		// PRIMEIRA CONDI��O - 1� e 4� CASO - B FICA FIXO
		// SEGUNDA CONDI��O - 2� e 3� CASO - A FICA FIXO
		
		if(equation == 1) {
			if(function == "f''(x) > 0" && fnFalseFirst(intervalo[0]) < 0 && fnFalseFirst(intervalo[1]) > 0 || function == "f''(x) < 0" && fnFalseFirst(intervalo[0]) > 0 && fnFalseFirst(intervalo[1]) < 0)
				fixo = intervalo[1];
			
			if(function == "f''(x) > 0" && fnFalseFirst(intervalo[0]) > 0 && fnFalseFirst(intervalo[1]) < 0 || function == "f''(x) < 0" && fnFalseFirst(intervalo[0]) < 0 && fnFalseFirst(intervalo[1]) > 0)
				fixo = intervalo[0];
		} else {
			if(function == "f''(x) > 0" && fnFalseSecond(intervalo[0]) < 0 && fnFalseSecond(intervalo[1]) > 0 || function == "f''(x) < 0" && fnFalseSecond(intervalo[0]) > 0 && fnFalseSecond(intervalo[1]) < 0)
				fixo = intervalo[0];
			
			if(function == "f''(x) > 0" && fnFalseSecond(intervalo[0]) > 0 && fnFalseSecond(intervalo[1]) < 0 || function == "f''(x) < 0" && fnFalseSecond(intervalo[0]) < 0 && fnFalseSecond(intervalo[1]) > 0)
				fixo = intervalo[1];
		}
		/*System.out.println(fixo);
		
		System.out.println(function);
		System.out.println(intervalo[0]);
		System.out.println(intervalo[1]);*/
		
		compararN_1 = fixo;
		
		
		do{
			breaks += "\nBreak: ("+intervalo[0]+" | "+fixo+")";
			
			intervalo[0] = fnFalsePosition(equation, intervalo[0], fixo);
			
			comparar1 = fnFalsePositionError(intervalo[0], compararN_1);
			breaks += " { Last Error = "+comparar1+" }";
			
			//System.out.println("++ ("+ intervalo[0]  +" || "+ fixo +")");
			
			compararN_1 = intervalo[0];
			
			if(equation == 2){
				if(intervalo[0] < fnFalsePosition(equation, intervalo[0], fixo)){
					breaks += "\nBreak: ("+intervalo[0]+" | "+fixo+")";
					
					intervalo[0] = fnFalsePosition(equation, intervalo[0], fixo);
					
					comparar1 = fnFalsePositionError(intervalo[0], compararN_1);
					breaks += " { Last Error = "+comparar1+" }";
					break;
				}
			}
			
		} while(comparar1 >= error);
		
		breaks += "\nLast Break ("+ intervalo[0] +" , "+ fnFalsePosition(equation, intervalo[0], fixo) +")";
		
		JOptionPane.showMessageDialog(null, breaks);

	}

	private static double fnFalseFirst(double x){
		return (Math.pow(Math.E, x) - 5);
	}
	private static double fnFalseSecond(double x){
		return (Math.pow(x, 3) - (2*x) + 3);
	}

	private static double fnFalseFirstDerivada2(double x){
		return (Math.pow(Math.E, x));
	}
	private static double fnFalseSecondDerivada2(double x){
		return (6*x);
	}
	
	// ((a*F(b))-(b*F(a)))/(F(b)-F(a))
	private static double fnFalsePosition(int equation, double a, double b){
		if(equation == 1)
			return (((a*fnFalseFirst(b))-(b*fnFalseFirst(a)))/(fnFalseFirst(b)-fnFalseFirst(a)));
		else
			return (((a*fnFalseSecond(b))-(b*fnFalseSecond(a)))/(fnFalseSecond(b)-fnFalseSecond(a)));
	}

	private static double fnFalsePositionError(double x, double y){
		return Math.abs(x-y);
	}
	
}
