package controller;

public class PolinomioTaylor {

	// Possui 3 ra�zes. Equation == 3, 5, 9, 13 e 14
	
	public static String rule = "", writePoli = "", resultPoli = "", realValue = "", error = "";

	public static void polinomioDeTaylor(int x, int n, int c, int qntH){        
		//RULE TAYLOR - Px(c) = f(c) + f(c)' (n-c)^1 + (f(c)''(n-c)^2)/2! + ... + (f(c)^n *(n-c)^n)/n!)

		double polinomioResul = Math.pow(Math.E, c);

		rule = "e^x";
		writePoli = "e^" + c;

		for(int i = 1; i <= n; i++){

			rule += " + e^x (x-Xo)";
			writePoli += " + e^" + c + " (" + x + "-" + c+ ")";

			if(i != 1){
				rule += "^"+ i + " / " + i + "!";
				writePoli += "^"+ i + " / " + i + "!";
			}

			polinomioResul += (Math.pow (Math.E, c) * Math.pow ((x - c), i) / factorial(i));
		}

		double valorReal =  Double.parseDouble(findDot("" + Math.pow (Math.E, x), qntH+1));

		rule = "P"+n+"(Xo): " + rule;
		writePoli = "P"+n+"("+c+"): " + writePoli;
		resultPoli = "Resultado do polin�mio: " + findDot(""+polinomioResul, qntH+1);
		realValue = "Valor Real: " + valorReal;
		error = "Erro: "+findDot(""+Math.abs(valorReal - polinomioResul), qntH+1);

	}
	public static String findDot(String error, int qntH){

		String [] dot = error.split("");

		for(int i=0; i<dot.length; i++){
			if(dot[i].equals("."))
				return error.substring(0, (i+qntH));
		}

		return error;
	}
	public static int factorial(int n){
		if (n==0)
			return 1;
		else
			return n*factorial(n-1);
	}

}