package controller;

public class Bisseccao {
	
	public static String metodoBisseccao2Roots(int equation, double erro){

		String x1 = "", x2 = "";
		double result[] = new double [10];
		double intervalo[] = new double[4];

		if(equation == 6)
			return toStringReturn(intervalo, equation);

		for(int i=0; i<result.length; i++){
			/*if(equation == 3 || equation == 5 || equation == 9 || equation == 13 || equation == 14)
				result[i] = chooseEquation(equation, i-4);
			else if (equation == 4)
				result[i] = chooseEquation(equation, i-2);
			else if (equation == 1 || equation == 2 || equation == 8 || equation == 15)
				result[i] = chooseEquation(equation, i-3);*/

			result[i] = chooseEquation(equation, i-3);

		}

		System.out.println("\nIntervalos = ");
		for(int i = 0; i<result.length; i++)
			System.out.println(result[i]);

		if(equation != 8){
			for(int i=0 ; i<result.length-1; i++){
				if(result[i] < 0 && result[i+1] > 0 || result[i] > 0 && result[i+1] < 0){
					if(x1.equals("")){
						intervalo [0] = (i-3);
						intervalo [1] = (i-2);
						x1 = "["+(i-3)+" | "+(i-2)+"]";
					}else if(x2.equals("")){
						intervalo [2] = (i-3);
						intervalo [3] = (i-2);
						x2 = "["+(i-3)+" | "+(i-2)+"]";
						if(equation==15){
							intervalo [2] = 4;
							intervalo [3] = 5;
							x2 = "[ 4 | 5]";
						}
						break;
					}
				} else if (result[i] == 0){
					if(x1.equals("")){
						intervalo [0] = (i-3);
						x1 = ""+(i-3);
					}else if(x2.equals("")){
						intervalo [1] = (i-3);
						x2 = ""+(i-3);
						break;
					}
				}
			}

			System.out.println(intervalo[0] + " = " +intervalo[1]);
			System.out.println(intervalo[2] + " = " +intervalo[3]);

			if(equation == 13){
				intervalo [0] = 2;
				intervalo [1] = 3;
			}

			for(int x=0; x<findQuantify(intervalo[0], intervalo[1], erro)+2; x++){

				for(int i=0; i<3; i++){
					if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) < 0 && chooseEquation(equation, intervalo[i]) > 0)
						intervalo[i+1] = (intervalo[i]+intervalo[i+1])/2;
					else if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) > 0 && chooseEquation(equation, intervalo[i]) < 0)
						intervalo[i+1] = (intervalo[i]+intervalo[i+1])/2;
					else if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) < 0 && chooseEquation(equation, intervalo[i+1]) > 0)
						intervalo[i] = (intervalo[i]+intervalo[i+1])/2;
					else if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) > 0 && chooseEquation(equation, intervalo[i+1]) < 0)
						intervalo[i] = (intervalo[i]+intervalo[i+1])/2;
					i++;
				}

				System.out.println("Novo intervalo 1 = ["+intervalo[0]+" | "+intervalo[1]+"]");
				System.out.println("Novo intervalo 2 = ["+intervalo[2]+" | "+intervalo[3]+"]");

			}
		}

		return toStringReturn(intervalo, equation);

	}
	public static String metodoBisseccao3Roots(int equation, double erro){

		String x1 = "", x2 = "", x3 = "";
		double result[] = new double [10];
		double intervalo[] = new double[6];

		for(int i=0; i<result.length; i++){
			if(equation==7 || equation == 11){
				if(equation==11)
					result[i] = chooseEquation(equation, i+1);
				else
					result[i] = chooseEquation(equation, i);
			}
			else 
				result[i] = chooseEquation(equation, i-4);
		}
		if(equation!=14){
			if(equation != 11){
				for(int i=0 ; i<result.length; i++){
					if(result[i] < 0 && result[i+1] > 0 || result[i] > 0 && result[i+1] < 0){
						if(x1.equals("")){
							intervalo [0] = (i-4);
							intervalo [1] = (i-3);
							x1 = "["+(i-4)+" | "+(i-3)+"]";
						}else if(x2.equals("")){
							intervalo [2] = (i-4);
							intervalo [3] = (i-3);
							x2 = "["+(i-4)+" | "+(i-3)+"]";
						}else if(x3.equals("")){
							intervalo [4] = (i-4);
							intervalo [5] = (i-3);
							x3 = "["+(i-4)+" | "+(i-3)+"]";
							break;
						}
					} else if (result[i] == 0){
						if(x1.equals("")){
							intervalo [0] = (i-4);
							x1 = ""+(i-4);
						}else if(x2.equals("")){
							intervalo [1] = (i-4);
							x2 = ""+(i-4);
						}else if(x3.equals("")){
							intervalo [2] = (i-4);
							x3 = ""+(i-4);
							break;
						}
					}
					if(equation==7)
						i++;
				}
			} else {
				intervalo[0] = 1;
				intervalo[1] = 2;
			}

			if(equation==7){
				intervalo[0] = 6;
				intervalo[1] = 7;
			}


			/*System.out.println(intervalo[0] + " = " +intervalo[1]);
			System.out.println(intervalo[2] + " = " +intervalo[3]);
			System.out.println(intervalo[4] + " = " +intervalo[5]);*/
			for(int x=0; x<findQuantify(intervalo[0], intervalo[1], erro); x++){

				for(int i=0; i<5; i++){
					if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) < 0 && chooseEquation(equation, intervalo[i]) > 0)
						intervalo[i+1] = (intervalo[i]+intervalo[i+1])/2;
					else if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) > 0 && chooseEquation(equation, intervalo[i]) < 0)
						intervalo[i+1] = (intervalo[i]+intervalo[i+1])/2;
					else if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) < 0 && chooseEquation(equation, intervalo[i+1]) > 0)
						intervalo[i] = (intervalo[i]+intervalo[i+1])/2;
					else if(chooseEquation(equation, (intervalo[i]+intervalo[i+1])/2) > 0 && chooseEquation(equation, intervalo[i+1]) < 0)
						intervalo[i] = (intervalo[i]+intervalo[i+1])/2;
					i++;
				}

				System.out.println("Novo intervalo 1 = ["+intervalo[0]+" | "+intervalo[1]+"]");
				System.out.println("Novo intervalo 2 = ["+intervalo[2]+" | "+intervalo[3]+"]");
				System.out.println("Novo intervalo 3 = ["+intervalo[4]+" | "+intervalo[5]+"]");

			}
		} else 
			intervalo[0] = -2;

		return toStringReturn(intervalo, equation);

	}
	public static double chooseEquation(int equation, double x){

		if(equation == 1)
			return fnFirst(x);
		if(equation == 2)
			return fnSecond(x);
		if(equation == 3)
			return fnThird(x);
		if(equation == 4)
			return fnFourth(x);
		if(equation == 5)
			return fnFiveth(x);
		if(equation == 6)
			return fnSixth(x);
		if(equation == 7)
			return fnSeventh(x);
		if(equation == 8)
			return fnEighth(x);
		if(equation == 9)
			return fnNinth(x);
		if(equation == 10)
			return fnTenth(x);
		if(equation == 11)
			return fnEleventh(x);
		if(equation == 12)
			return fnTwelfth(x);
		if(equation == 13)
			return fnThirteenth(x);
		if(equation == 14)
			return fnFourteenth(x);
		if(equation == 15)
			return fnFiveteenth(x);

		return 0;
	}

	public static int findQuantify(double a, double b, double erro){
		return (int) Math.round((Math.log10(b-a)-Math.log10(erro))/Math.log10(2));
	}

	private static double fnFirst(double x){
		return (Math.pow((x+1), 2)*Math.pow(Math.E, (Math.pow(x, 2)-2)) - 1); 
	}
	private static double fnSecond(double x){
		return (Math.pow(x, 2) - x - 2); 
	}
	private static double fnThird(double x){
		return (Math.pow(x, 3) - 9*x + 3);
	}
	private static double fnFourth(double x){
		return (4 * Math.sin(x) - Math.pow(Math.E, x));
	}
	private static double fnFiveth(double x){
		return (Math.pow(x, 3) - 9*x + 3);
	}
	private static double fnSixth(double x){
		return (Math.sqrt(x) - 5 * Math.pow(Math.E, x));
	}
	private static double fnSeventh(double x){
		return (x * Math.log10(x) - 5);
	}
	private static double fnEighth(double x){
		return (Math.pow(Math.E, Math.pow(-x,2)) - Math.cos(x));
	}
	private static double fnNinth(double x){
		return (Math.pow(x, 3) - x - 1);
	}
	private static double fnTenth(double x){
		return (4 * Math.cos(x) - Math.pow(Math.E, 2*x));
	}
	private static double fnEleventh(double x){
		return (1 - x * Math.log(x));
	}
	private static double fnTwelfth(double x){
		return (Math.pow(2, x) - 3*x);
	}
	private static double fnThirteenth(double x){
		return (Math.pow(x, 3) - x - 10);
	}
	private static double fnFourteenth(double x){
		return (Math.pow(x, 3) - 2 * Math.pow(x, 2) - 3*x + 10);
	}
	private static double fnFiveteenth(double x){
		return (Math.pow(Math.E, x) - 4 * Math.pow(x, 2));
	}

	private static String toStringReturn(double intervalo[], int equation){

		String strInter = "Intervalos: \n";

		if(equation == 3 || equation == 5)
			equation = 6;
		else if (equation == 2 || equation == 13 || equation == 4 || equation == 12 || equation == 7 || equation == 9 || equation == 11){
			if(equation == 2)
				strInter = "Ra�zes: \n";
			equation = 2;
		} else if (equation == 8)
			return "Ra�z \n[ 0.0 ]";
		else if (equation == 14)
			return "Ra�z \n[ "+intervalo[0]+" ]";
		else if (equation == 6)
			return "N�o foi poss�vel encontrar valores reais para essa fun��o!";
		else
			equation = 4;


		for(int i=1; i<equation; i++){
			strInter += "[ "+intervalo[i-1] +" | "+ intervalo[i]+" ] \n";
			i++;
		}

		return strInter;
	}
}
