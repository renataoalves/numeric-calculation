package controller;

import java.text.DecimalFormat;

import javax.swing.JOptionPane;

public class Newton {

	private static String resultados = "Intervalos 1� raiz:\n", resultadoMaisRaiz = "\n";
	private static boolean proximo1 = true, umaVez = true;
	private static int identificador = 0, maisVezes = 0;
	
	public static void metodoCalcularNewton(int equacao, String erroDaTela) {
		try {
			
			double erro = Double.parseDouble(erroDaTela.trim());
			double melhorExtremo = 0.0;
			int raiz1, raiz2;
			
			if (equacao == 1) {
				raiz1 = 2;
				raiz2 = 3;
				melhorExtremo = fnMelhorExtremo(fnOne(raiz1), fnOneDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 2) {
				int []raiz = {-1, 0, 1, 2, 4, 5};
				if(identificador == 0){
					melhorExtremo = fnMelhorExtremo(fnTwo(raiz[0]), fnTwoDeriva2(raiz[0])) ? raiz[0] : raiz[1];
					identificador = 1;
				} else if(identificador == 1){
					melhorExtremo = fnMelhorExtremo(fnTwo(raiz[2]), fnTwoDeriva2(raiz[2])) ? raiz[2] : raiz[3];
					identificador = 2;
				} else {
					melhorExtremo = fnMelhorExtremo(fnTwo(raiz[4]), fnTwoDeriva2(raiz[4])) ? raiz[4] : raiz[5];
					identificador = 0;
				}
			} else if (equacao == 3) {
				raiz1 = 1;
				raiz2 = 2;
				melhorExtremo = fnMelhorExtremo(fnThree(raiz1), fnThreeDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 4) {
				int []raiz = {8, 9, 1, 2};
				if(proximo1){
					melhorExtremo = fnMelhorExtremo(fnFour(raiz[0]), fnFourDeriva2(raiz[0])) ? raiz[0] : raiz[1];
					proximo1 = false;
				} else {
					melhorExtremo = fnMelhorExtremo(fnFour(raiz[2]), fnFourDeriva2(raiz[2])) ? raiz[2] : raiz[3];
					proximo1 = true;
				}
			} else if (equacao == 5) {
				raiz1 = 1;
				raiz2 = 2;
				melhorExtremo = fnMelhorExtremo(fnFive(raiz1), fnFiveDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 6) {
				raiz1 = -1;
				raiz2 = -2;
				melhorExtremo = fnMelhorExtremo(fnSix(raiz1), fnSixDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 7) {
				raiz1 = 0;
				raiz2 = 1;
				melhorExtremo = fnMelhorExtremo(fnSeven(raiz1), fnSevenDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 8) {
				raiz1 = 1;
				raiz2 = 2;
				melhorExtremo = fnMelhorExtremo(fnEight(raiz1), fnEightDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 9) {
				raiz1 = 0;
				raiz2 = 1;
				melhorExtremo = fnMelhorExtremo(fnNine(raiz1), fnNineDeriva2(raiz1)) ? raiz1 : raiz2;
			} else if (equacao == 10) {
				raiz1 = -1;
				raiz2 = 0;
				melhorExtremo = fnMelhorExtremo(fnTen(raiz1), fnTenDeriva2(raiz1)) ? raiz1 : raiz2;
			}

			fnCalculaNewton(equacao, melhorExtremo, erro);

		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "N�MERO INV�LIDO!", "ERRO", 0);
		}

	}

	public static void fnCalculaNewton(int equacao, double melhorExtremo, double erro) {
		double result = 0.0;
		int cont = 1;
		
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("0.0000");
		
		if(equacao == 4 && !umaVez)
			resultadoMaisRaiz += "Intervalos 2� raiz:\n";
		
		if(equacao == 2 && maisVezes == 1)
			resultadoMaisRaiz += "Intervalos 2� raiz:\n";
		
		if(equacao == 2 && maisVezes == 2)
			resultadoMaisRaiz += "Intervalos 3� raiz:\n";

		do {
			result = melhorExtremo;

			if (equacao == 1)
				melhorExtremo = fnNewton(melhorExtremo, fnOne(melhorExtremo), fnOneDeriva1(melhorExtremo));
			else if (equacao == 2)
				melhorExtremo = fnNewton(melhorExtremo, fnTwo(melhorExtremo), fnTwoDeriva1(melhorExtremo));
			else if (equacao == 3)
				melhorExtremo = fnNewton(melhorExtremo, fnThree(melhorExtremo), fnThreeDeriva1(melhorExtremo));
			else if (equacao == 4)
				melhorExtremo = fnNewton(melhorExtremo, fnFour(melhorExtremo), fnFourDeriva1(melhorExtremo));
			else if (equacao == 5)
				melhorExtremo = fnNewton(melhorExtremo, fnFive(melhorExtremo), fnFiveDeriva1(melhorExtremo));
			else if (equacao == 6)
				melhorExtremo = fnNewton(melhorExtremo, fnSix(melhorExtremo), fnSixDeriva1(melhorExtremo));
			else if (equacao == 7)
				melhorExtremo = fnNewton(melhorExtremo, fnSeven(melhorExtremo), fnSevenDeriva1(melhorExtremo));
			else if (equacao == 8)
				melhorExtremo = fnNewton(melhorExtremo, fnEight(melhorExtremo), fnEightDeriva1(melhorExtremo));
			else if (equacao == 9)
				melhorExtremo = fnNewton(melhorExtremo, fnNine(melhorExtremo), fnNineDeriva1(melhorExtremo));
			else if (equacao == 10)
				melhorExtremo = fnNewton(melhorExtremo, fnTen(melhorExtremo), fnTenDeriva1(melhorExtremo));
			
			if (Math.abs(result - melhorExtremo) >= erro) {
				if(equacao != 4 && equacao != 2)
					resultados += "x" + cont + ". [" + df.format(melhorExtremo) + " ; " + df.format(result) + "]\n";
				else
					resultadoMaisRaiz += "x" + cont + ". [" + df.format(melhorExtremo) + " ; " + df.format(result) + "]\n";
				cont++;
			}
		} while (Math.abs(result - melhorExtremo) >= erro);
		
		if (melhorExtremo < result){
			if (equacao == 2 || equacao == 4)
				resultadoMaisRaiz += "\nIntervalo Final = [" + df.format(melhorExtremo) + " ; " + df.format(result) + "]\n\n";
			else
				resultados += "\nIntervalo Final = [" + df.format(melhorExtremo) + " ; " + df.format(result) + "]\n\n";
		}else{
			if (equacao == 2 || equacao == 4)
				resultadoMaisRaiz += "\nIntervalo Final = [" + df.format(result) + " ; " + df.format(melhorExtremo) + "]\n\n";
			else 
				resultados += "\nIntervalo Final = [" + df.format(result) + " ; " + df.format(melhorExtremo) + "]\n\n";
		}
		
		if(equacao == 4 && umaVez){
			umaVez = false;
			metodoCalcularNewton(equacao, ""+erro);
		}
		umaVez = true;
		
		if(equacao == 2 && maisVezes != 2){
			maisVezes++;
			metodoCalcularNewton(equacao, ""+erro);
		}
		maisVezes = 0;
			
		JOptionPane.showMessageDialog(null, resultados+resultadoMaisRaiz);
		
		resultados = "Intervalos 1� raiz:\n";
		resultadoMaisRaiz = "\n";
	}

	public static double fnOne(double x) {
		return (2 * x - Math.sin(x) - 4);
	}
	public static double fnOneDeriva1(double x) {
		return (2 - Math.cos(x));
	}
	public static double fnOneDeriva2(double x) {
		return (Math.sin(x));
	}

	public static double fnTwo(double x) {
		return (Math.pow(x, 3) - 5 * Math.pow(x, 2) + x + 3);
	}
	public static double fnTwoDeriva1(double x) {
		return (3 * Math.pow(x, 2) - 10 * x + 1);
	}
	public static double fnTwoDeriva2(double x) {
		return (6 * x + 10);
	}

	public static double fnThree(double x) {
		return (Math.pow(x, 3) - 5);
	}
	public static double fnThreeDeriva1(double x) {
		return (3 * Math.pow(x, 2));
	}
	public static double fnThreeDeriva2(double x) {
		return (6 * x);
	}

	public static double fnFour(double x) {
		return (Math.pow(x, 2) - 9.5 * x + 8.5);
	}
	public static double fnFourDeriva1(double x) {
		return (2 * x - 9.5);
	}
	public static double fnFourDeriva2(double x) {
		return (2);
	}

	public static double fnFive(double x) {
		return (Math.pow(x, 5) - 3.7 * Math.pow(x, 4) + 7.4 * Math.pow(x, 3) - 10.8 * Math.pow(x, 2) + 10.8 * x - 6.8);
	}
	public static double fnFiveDeriva1(double x) {
		return (5 * Math.pow(x, 4) - 14.8 * Math.pow(x, 3) + 22.2 * Math.pow(x, 2) - 21 * x + 10.8);
	}
	public static double fnFiveDeriva2(double x) {
		return (20 * Math.pow(x, 3) - 44.4 * Math.pow(x, 2) + 44.4 * x - 21);
	}

	public static double fnSix(double x) {
		return (Math.pow(x, 3) - x + 1);
	}
	public static double fnSixDeriva1(double x) {
		return (3 * Math.pow(x, 2) - 1);
	}
	public static double fnSixDeriva2(double x) {
		return (6 * x);
	}

	public static double fnSeven(double x) {
		return (3 * x - Math.cos(x));
	}
	public static double fnSevenDeriva1(double x) {
		return (x + Math.sin(x));
	}
	public static double fnSevenDeriva2(double x) {
		return (1 + Math.cos(x));
	}

	public static double fnEight(double x) {
		return (Math.pow(x, 3) - 4);
	}
	public static double fnEightDeriva1(double x) {
		return (3 * Math.pow(x, 2));
	}
	public static double fnEightDeriva2(double x) {
		return (6 * x);
	}

	public static double fnNine(double x) {
		return (10 * Math.sin(x) + Math.cos(x) - 10 * x);
	}
	public static double fnNineDeriva1(double x) {
		return (Math.cos(x) - Math.sin(x) - 10);
	}
	public static double fnNineDeriva2(double x) {
		return (-Math.sin(x) - Math.cos(x));
	}

	public static double fnTen(double x) {
		return (3 * Math.pow(x, 5) - 2 * Math.pow(x, 4) - Math.pow(x, 3) - 2 * x + 1);
	}
	public static double fnTenDeriva1(double x) {
		return (15 * Math.pow(x, 4) - 8 * Math.pow(x, 3) - 3 * Math.pow(x, 2) - 2);
	}
	public static double fnTenDeriva2(double x) {
		return (60 * Math.pow(x, 3) - 24 * Math.pow(x, 2) - 6 * x);
	}

	public static boolean fnMelhorExtremo(double f1, double f2) {
		return f1 > 0 && f2 > 0 || f1 < 0 && f2 < 0;
	}

	public static double fnNewton(double x, double funcao, double derivada1funcao) {
		return (x - (funcao / derivada1funcao));
	}

}
